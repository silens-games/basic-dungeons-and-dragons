package components.swing_gui.character_equipment_carried;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@RequiredArgsConstructor
public class CharacterEquipmentCarriedModel {
    private final List<String> equipmentList;

    public CharacterEquipmentCarriedModel() {
        this(
                new ArrayList<>()
        );
    }
}
