package components.swing_gui.character_saving_throws;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class CharacterSavingThrowsModels {
    private final int deathRayOrPoison;
    private final int magicWands;
    private final int ParalysisOrTurnToStone;
    private final int DragonBreath;
    private final int RodStaffOrSpell;

    public CharacterSavingThrowsModels() {
        this(
                0,
                0,
                0,
                0,
                0
        );
    }
}
