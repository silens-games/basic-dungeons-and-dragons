package components.swing_gui.character_description;

import components.swing_gui.SwingController;
import lombok.Getter;

import javax.swing.*;

public class CharacterDescriptionPresenter implements CharacterDescriptionContract.Presenter {

    @Getter
    private final CharacterDescriptionView characterDescriptionView;

    private final SwingController swingController;

    public CharacterDescriptionPresenter(SwingController swingController) {
        this.swingController = swingController;

        characterDescriptionView = new CharacterDescriptionView();
    }

    @Override
    public void initialize() {
        configureView();

        addListeners();
    }

    private void configureView() {
        characterDescriptionView.initialize();
    }

    private void addListeners() {
//        outputButtonListener();
//
//        closeButtonListener();
    }

//    private void outputButtonListener() {
//        characterDescriptionView.getExportButton()
//                .addActionListener(e -> {
//                    onOutput();
//                });
//    }

    @Override
    public void onOutput() {
        swingController.print();
    }

//    private void closeButtonListener() {
//        characterDescriptionView.getCloseButton()
//                .addActionListener(e -> {
//                    onClose();
//                });
//    }


    @Override
    public void onClose() {
        SwingUtilities.getWindowAncestor(characterDescriptionView).dispose();
    }
}
