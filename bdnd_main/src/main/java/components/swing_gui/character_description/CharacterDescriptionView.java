package components.swing_gui.character_description;

import lombok.Getter;

import javax.swing.*;
import java.awt.*;

public class CharacterDescriptionView extends JPanel implements CharacterDescriptionContract.View {

    private GridBagLayout gridBagLayout;

    private GridBagConstraints gridBagConstraints;

    @Getter
    private JTextField nameTextField;
    @Getter
    private JTextField playerNameTextField;
    @Getter
    private JTextField dungeonMasterTextField;
    @Getter
    private JTextField clazzTextField;
    @Getter
    private JTextField alignmentTextField;
    @Getter
    private JTextField levelTextField;

    @Getter
    private JTextField raceTextField;
    @Getter
    private JTextField heightTextField;
    @Getter
    private JTextField weightTextField;
    @Getter
    private JTextField  ageTextField;
    @Getter
    private JTextField hairTextField;
    @Getter
    private JTextField eyeTextField;

    public CharacterDescriptionView() {
        gridBagLayout = new GridBagLayout();
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.insets = new Insets(5, 5, 5, 5);

        this.setLayout(gridBagLayout);
    }

    @Override
    public void initialize() {
        configureMainJPanel();

        addRowOne();

        addRowTwo();

        addRowThree();

        addRowFour();

        addRowFive();

        addRowSix();
    }

    private void configureMainJPanel() {
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;

        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
    }

    private void addRowOne(){
        gridBagConstraints.gridx= 0;

        addCharacterNameLabel();

        addCharacterNameTextField();

        addCharacterClassLabel();

        addCharacterClassTextField();

        gridBagConstraints.gridy++;
    }

    private void addCharacterNameLabel() {
        gridBagConstraints.weightx = 1;

        JLabel label = new JLabel("Character's Name:");

        this.add(label, gridBagConstraints);

        gridBagConstraints.gridx++;
    }

    private void addCharacterNameTextField() {
        gridBagConstraints.weightx = 0;

        nameTextField = new JTextField(20);

        this.add(nameTextField, gridBagConstraints);

        gridBagConstraints.gridx++;
    }

    private void addCharacterClassLabel() {
        gridBagConstraints.weightx = 1;

        JLabel label = new JLabel("Class:");

        this.add(label, gridBagConstraints);

        gridBagConstraints.gridx++;
    }

    private void addCharacterClassTextField() {
        gridBagConstraints.weightx = 0;

        clazzTextField = new JTextField(20);

        this.add(clazzTextField, gridBagConstraints);

        gridBagConstraints.gridx++;
    }

    private void addRowTwo(){
        gridBagConstraints.gridx= 0;

        addPlayerNameLabel();

        addPlayerNameTextField();

        addAlignmentLabel();

        addAlignmentTextField();

        gridBagConstraints.gridy++;
    }

    private void addPlayerNameLabel() {
        gridBagConstraints.weightx = 1;

        JLabel label = new JLabel("Player's Name");

        this.add(label, gridBagConstraints);

        gridBagConstraints.gridx++;
    }

    private void addPlayerNameTextField() {
        gridBagConstraints.weightx = 0;

        playerNameTextField = new JTextField(20);

        this.add(playerNameTextField, gridBagConstraints);

        gridBagConstraints.gridx++;
    }

    private void addAlignmentLabel() {
        gridBagConstraints.weightx = 1;

        JLabel label = new JLabel("Alignment");

        this.add(label, gridBagConstraints);

        gridBagConstraints.gridx++;
    }

    private void addAlignmentTextField() {
        gridBagConstraints.weightx = 0;

        alignmentTextField = new JTextField(20);

        this.add(alignmentTextField, gridBagConstraints);

        gridBagConstraints.gridx++;
    }

    private void addRowThree(){
        gridBagConstraints.gridx= 0;

        addDungeonMasterLabel();

        addDungeonMasterTextField();

        addLevelLabel();

        addLevelTextField();

        gridBagConstraints.gridy++;
    }

    private void addDungeonMasterLabel() {
        gridBagConstraints.weightx = 1;

        JLabel label = new JLabel("Dungeon Maser:");

        this.add(label, gridBagConstraints);

        gridBagConstraints.gridx++;
    }

    private void addDungeonMasterTextField() {
        gridBagConstraints.weightx = 0;

        dungeonMasterTextField = new JTextField(20);

        this.add(dungeonMasterTextField, gridBagConstraints);

        gridBagConstraints.gridx++;
    }

    private void addLevelLabel() {
        gridBagConstraints.weightx = 1;

        JLabel label = new JLabel("Level:");

        this.add(label, gridBagConstraints);

        gridBagConstraints.gridx++;
    }

    private void addLevelTextField() {
        gridBagConstraints.weightx = 0;

        levelTextField = new JTextField(20);

        this.add(levelTextField, gridBagConstraints);

        gridBagConstraints.gridx++;
    }

    private void addRowFour(){
        gridBagConstraints.gridx= 0;

        addRaceLabel();

        addRaceTextField();

        addAgeLabel();

        addAgeTextField();

        gridBagConstraints.gridy++;
    }

    private void addRaceLabel() {
        gridBagConstraints.weightx = 1;

        JLabel label = new JLabel("Race:");

        this.add(label, gridBagConstraints);

        gridBagConstraints.gridx++;
    }

    private void addRaceTextField() {
        gridBagConstraints.weightx = 0;

        raceTextField = new JTextField(20);

        this.add(raceTextField, gridBagConstraints);

        gridBagConstraints.gridx++;
    }

    private void addAgeLabel() {
        gridBagConstraints.weightx = 1;

        JLabel label = new JLabel("Age:");

        this.add(label, gridBagConstraints);

        gridBagConstraints.gridx++;
    }

    private void addAgeTextField() {
        gridBagConstraints.weightx = 0;

        ageTextField = new JTextField(20);

        this.add(ageTextField, gridBagConstraints);

        gridBagConstraints.gridx++;
    }

    private void addRowFive(){
        gridBagConstraints.gridx= 0;

        addHeightLabel();

        addHeightTextField();

        addHairLabel();

        addHairTextField();

        gridBagConstraints.gridy++;
    }

    private void addHeightLabel() {
        gridBagConstraints.weightx = 1;

        JLabel label = new JLabel("Height:");

        this.add(label, gridBagConstraints);

        gridBagConstraints.gridx++;
    }

    private void addHeightTextField() {
        gridBagConstraints.weightx = 0;

        heightTextField = new JTextField(20);

        this.add(heightTextField, gridBagConstraints);

        gridBagConstraints.gridx++;
    }

    private void addHairLabel() {
        gridBagConstraints.weightx = 1;

        JLabel label = new JLabel("Hair:");

        this.add(label, gridBagConstraints);

        gridBagConstraints.gridx++;
    }

    private void addHairTextField() {
        gridBagConstraints.weightx = 0;

        hairTextField = new JTextField(20);

        this.add(hairTextField, gridBagConstraints);

        gridBagConstraints.gridx++;
    }

    private void addRowSix(){
        gridBagConstraints.gridx= 0;

        addHWightLabel();

        addWeightTextField();

        addEyesLabel();

        addEyesTextField();

        gridBagConstraints.gridy++;
    }

    private void addHWightLabel() {
        gridBagConstraints.weightx = 1;

        JLabel label = new JLabel("Weight:");

        this.add(label, gridBagConstraints);

        gridBagConstraints.gridx++;
    }

    private void addWeightTextField() {
        gridBagConstraints.weightx = 0;

        weightTextField = new JTextField(20);

        this.add(weightTextField, gridBagConstraints);

        gridBagConstraints.gridx++;
    }

    private void addEyesLabel() {
        gridBagConstraints.weightx = 1;

        JLabel label = new JLabel("Eyes:");

        this.add(label, gridBagConstraints);

        gridBagConstraints.gridx++;
    }

    private void addEyesTextField() {
        gridBagConstraints.weightx = 0;

        eyeTextField = new JTextField(20);

        this.add(eyeTextField, gridBagConstraints);

        gridBagConstraints.gridx++;
    }
}
