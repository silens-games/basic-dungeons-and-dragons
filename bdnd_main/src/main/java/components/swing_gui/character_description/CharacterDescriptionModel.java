package components.swing_gui.character_description;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class CharacterDescriptionModel {
    private final String name;
    private final String playerName;
    private final String dungeonMaster;
    private final String clazz;
    private final String alignment;
    private final String level;

    private final String race;
    private final int height;
    private final int weight;
    private final int age;
    private final String hair;
    private final String eye;

    public CharacterDescriptionModel() {
        this(
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                0,
                0,
                0,
                "",
                ""
                );
    }
}
