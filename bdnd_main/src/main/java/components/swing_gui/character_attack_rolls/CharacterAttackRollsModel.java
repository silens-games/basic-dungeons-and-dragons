package components.swing_gui.character_attack_rolls;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@Builder
@RequiredArgsConstructor
public class CharacterAttackRollsModel {
    private final Map<ArmorClass, AttackRollNeeded> attackRolls;
    private final int thaco;

    public CharacterAttackRollsModel() {
        this(
                new HashMap<>(),
                0
        );
    }

    @Builder
    protected static class ArmorClass{
        public int value;
    }

    @Builder
    protected static class AttackRollNeeded{
        public int value;
    }
}
