package components.swing_gui;

import components.swing_gui.character_chooser.CharacterChooserView;
import components.swing_gui.character_description.CharacterDescriptionView;
import logger.Log;
import lombok.Getter;

import javax.swing.*;
import java.awt.*;

public class SwingView extends JPanel {
  private final Log logger = new Log();

  private final GridBagLayout gridBagLayout;
  private final GridBagConstraints gridBagConstraints;

  @Getter
  private final CharacterChooserView characterChooserView;

  @Getter
  private final CharacterDescriptionView characterDescriptionView;

//  @Getter
//  private final TableView tableView;

//  @Getter
//  private final EntityOutputView EntityOutputView;


  public SwingView(CharacterChooserView CharacterChooserView, CharacterDescriptionView characterDescriptionView) {
    this.gridBagLayout = new GridBagLayout();
    this.gridBagConstraints = new GridBagConstraints();

    this.characterChooserView = CharacterChooserView;
    this.characterDescriptionView = characterDescriptionView;
//    this.tableView = tableView;
//    this.EntityOutputView = EntityOutputView;
  }

  public void initialize() {
    configureMainJPanel();

    addCharacterDescriptionPanel();

    addEntityChooserPanel();

    addTableViewPanel();

//    addEntityOutputViewPanel();
  }

  private void configureMainJPanel() {
    this.setLayout(gridBagLayout);

    gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
  }

  private void addCharacterDescriptionPanel() {
    gridBagConstraints.gridwidth = 8;
    gridBagConstraints.weightx = 1;
    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy++;

    characterDescriptionView.initialize();

    this.add(characterDescriptionView, gridBagConstraints);
  }

  private void addEntityChooserPanel() {
    gridBagConstraints.gridwidth = 8;
    gridBagConstraints.weightx = 1;
    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy++;

    this.add(characterChooserView, gridBagConstraints);
  }

  private void addTableViewPanel() {
//    gridBagConstraints.gridwidth = 8;
//    gridBagConstraints.weightx = 1;
//    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
//    gridBagConstraints.gridx = 0;
//    gridBagConstraints.gridy++;
//
//    this.add(tableView, gridBagConstraints);
  }

//  private void addEntityOutputViewPanel() {
//    gridBagConstraints.gridwidth = 8;
//    gridBagConstraints.weightx = 1;
//    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
//    gridBagConstraints.gridx = 0;
//    gridBagConstraints.gridy++;
//
//    this.add(EntityOutputView, gridBagConstraints);
//  }
}
