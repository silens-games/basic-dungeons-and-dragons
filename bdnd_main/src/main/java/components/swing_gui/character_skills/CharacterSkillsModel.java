package components.swing_gui.character_skills;


import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@RequiredArgsConstructor
public class CharacterSkillsModel {
    private final List<String> equipmentList;

    public CharacterSkillsModel() {
        this(
                new ArrayList<>()
        );
    }
}
