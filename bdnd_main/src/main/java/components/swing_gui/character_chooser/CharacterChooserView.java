package components.swing_gui.character_chooser;

import logger.Log;

import javax.swing.*;
import java.awt.*;

public class CharacterChooserView extends JPanel implements CharacterChooserContract.View {
  private final Log logger = new Log();

  private final GridBagLayout gridBagLayout;
  private final GridBagConstraints gridBagConstraints;

//  private JComboBox<IngestorType> ingestorTypeJComboBox;

  private JButton ingestButton;

  public CharacterChooserView() {
    this.gridBagLayout = new GridBagLayout();
    this.gridBagConstraints = new GridBagConstraints();
  }

  @Override
  public void initialize() {
    configureMainJPanel();

    addIngestorType();

    addIngestButton();

    addHorizontalSpaceLabel();
  }

  private void configureMainJPanel() {
    this.setLayout(gridBagLayout);

    gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
    gridBagConstraints.insets = new Insets(5, 5, 5, 5);

    gridBagConstraints.gridy = 0;
    gridBagConstraints.gridx = 0;
  }

  private void addIngestorType() {
    addIngestorTypeLabel();

//    addIngestorTypeJComboBox();
  }

  private void addIngestorTypeLabel() {
    gridBagConstraints.weightx = 0;

    JLabel ingestorTypeLabel = new JLabel("Choose input file format: ");

    this.add(ingestorTypeLabel, gridBagConstraints);

    gridBagConstraints.gridx++;
  }

//  private void addIngestorTypeJComboBox() {
//    gridBagConstraints.weightx = 0;
//
//    ingestorTypeJComboBox = new JComboBox(IngestorType.values());
//    ingestorTypeJComboBox.setSelectedIndex(0);
//    ingestorTypeJComboBox.setRenderer(ingestorTypeRenderer());
//
//    this.add(ingestorTypeJComboBox, gridBagConstraints);
//
//    gridBagConstraints.gridx++;
//  }

//  public void setComboBoxLength() {
//    int longestItemLength = 0;
//    for (IngestorType item : IngestorType.values()) {
//      longestItemLength = Math.max(longestItemLength, item.toString().length());
//    }
//
//    ingestorTypeJComboBox.setMaximumSize(new Dimension((longestItemLength * 10) + 20, ingestorTypeJComboBox.getPreferredSize().height));
//  }

//  private ListCellRenderer<IngestorType> ingestorTypeRenderer(){
//    return new ListCellRenderer<IngestorType>() {
//      @Override
//      public Component getListCellRendererComponent(JList<? extends IngestorType> list, IngestorType value, int index, boolean isSelected, boolean cellHasFocus) {
//        JLabel label = new JLabel(value.toString());
//        label.setPreferredSize(new Dimension(value.toString().length() * 10, label.getPreferredSize().height));
//        return label;
//      }
//    };
//  }

  private void addIngestButton() {
    gridBagConstraints.weightx = 0;

    ingestButton = new JButton("Ingest");

    this.add(ingestButton, gridBagConstraints);

    gridBagConstraints.gridx++;
  }

  private void addHorizontalSpaceLabel() {
    gridBagConstraints.weightx = 1;

    JLabel label = new JLabel("");

    this.add(label, gridBagConstraints);

    gridBagConstraints.weightx = 0;
    gridBagConstraints.gridx++;
  }

//  @Override
//  public IngestorType getIngestorType() {
//    return (IngestorType) ingestorTypeJComboBox.getSelectedItem();
//  }

  @Override
  public JButton getIngestButton() {
    return ingestButton;
  }
}
