package components.swing_gui.character_chooser;

import logger.Log;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CharacterButtonListener implements ActionListener {
  private final Log logger = new Log(this.getClass());

  private final CharacterChooserPresenter characterChooserPresenter;

  public CharacterButtonListener(CharacterChooserPresenter characterChooserPresenter) {
    this.characterChooserPresenter = characterChooserPresenter;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    characterChooserPresenter.onIngest();
  }
}