package components.swing_gui.character_chooser;

import file.file_chooser.FileChooser;
import lombok.Getter;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CharacterChooserPresenter implements CharacterChooserContract.Presenter {

  public final static Path FILE_PATH = Paths.get(System.getProperty("user.home") + File.separator +
      "AppData" + File.separator +
      "Local" + File.separator +
      "dnd" + File.separator +
      "bdnd.json");

  private FileChooser fileChooser;

  @Getter
  private final CharacterChooserView characterChooserView;

  public CharacterChooserPresenter(FileChooser fileChooser) {
    this.fileChooser = fileChooser;
    this.characterChooserView = new CharacterChooserView();
  }

  @Override
  public void initialize() {
    configureView();

    addListeners();
  }

  private void configureView() {
    characterChooserView.initialize();
  }

  private void addListeners() {
    addIngestButtonListener();
  }

  private void addIngestButtonListener() {
    characterChooserView.getIngestButton().addActionListener(new CharacterButtonListener(this));
  }

  @Override
  public void onIngest() {
    Path path = fileChooser.open(FILE_PATH.toString()).toPath();
    if (path.toFile().exists()) {

    }
  }
}
