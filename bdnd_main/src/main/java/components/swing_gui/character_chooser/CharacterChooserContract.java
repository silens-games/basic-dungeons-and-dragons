package components.swing_gui.character_chooser;

import javax.swing.*;

public interface CharacterChooserContract {
  interface Presenter {
    void initialize();

    void onIngest();
  }

  interface View {
    void initialize();

    JButton getIngestButton();
  }
}
