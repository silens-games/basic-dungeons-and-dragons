package components.swing_gui.character_movement_rates;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class CharacterMovementRatesModel {
    private final int normal;
    private final int encounter;
    private final int running;
    private final int other;

    public CharacterMovementRatesModel() {
        this(
                0,
                0,
                0,
                0
        );
    }
}
