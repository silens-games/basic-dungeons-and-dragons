package components.swing_gui;

import components.swing_gui.character_chooser.CharacterChooserPresenter;
import components.swing_gui.character_description.CharacterDescriptionPresenter;
import contracts.gui_contract.Gui;
import contracts.gui_contract.GuiDto;
import file.file_chooser.FileChooser;
import file.file_chooser.FileChooserImp;
import logger.Log;
import lombok.Getter;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SwingController implements Gui {
    private Log logger = new Log();

    public final static Path SAVE_PATH = Paths.get(System.getProperty("user.home") + File.separator +
            "AppData" + File.separator +
            "Local" + File.separator +
            "template" + File.separator +
            "template.csv");

    private JFrame frame;

    @Getter
    private SwingView swingView;

    private final CharacterChooserPresenter characterChooserPresenter;

    private final CharacterDescriptionPresenter characterDescriptionPresenter;

//  private final TablePresenter tablePresenter;

//  private final EntityOutputPresenter EntityOutputPresenter;


    public SwingController() {

        characterDescriptionPresenter = new CharacterDescriptionPresenter(this);

        FileChooser fileChooser = new FileChooserImp();
        characterChooserPresenter = new CharacterChooserPresenter(fileChooser);
//    EntityChooserPresenter.initialize();

//    tablePresenter = new TablePresenter();
//    tablePresenter.initialize();

//    EntityOutputPresenter = new EntityOutputPresenter(this);
//    EntityOutputPresenter.initialize();
    }

    @Override
    public void initialize() {
        swingView = new SwingView(
                characterChooserPresenter.getCharacterChooserView(),
                characterDescriptionPresenter.getCharacterDescriptionView()
        );
        swingView.initialize();

        drawFrame(swingView);
    }

    private void drawFrame(SwingView swingView) {
        frame = new JFrame("Basic D&D Character Sheet");
        frame.setContentPane(swingView);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    @Override
    public void clearView() {
//    tablePresenter.clearView();
    }

    @Override
    public void setView(GuiDto guiDto) {
//    tablePresenter.setView(guiDto);
    }

    @Override
    public GuiDto getModel() {
//    return tablePresenter.getModel();
        return null;
    }

    public void print() {
        FileChooser fileChooser = new FileChooserImp();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("CSV", "csv");

//    mediator.print(fileChooser.save(SAVE_PATH.toString(), filter).toPath(), getModel());
    }
}
