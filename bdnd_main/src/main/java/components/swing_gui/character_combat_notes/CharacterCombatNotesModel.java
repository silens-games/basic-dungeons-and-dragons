package components.swing_gui.character_combat_notes;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class CharacterCombatNotesModel {
    private final int armorClass;
    private final int hitPoints;
    private final int damage;

    public CharacterCombatNotesModel() {
        this(
                0,
                0,
                0
        );
    }
}
