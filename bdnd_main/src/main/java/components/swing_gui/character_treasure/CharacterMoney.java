package components.swing_gui.character_treasure;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@RequiredArgsConstructor
public class CharacterMoney {
    private final long platinumPieces;
    private final long goldPieces;
    private final long electrumPieces;
    private final long silverPieces;
    private final long copperPieces;
    private final List<String> gems;

    public CharacterMoney() {
        this(
                0L,
                0L,
                0L,
                0L,
                0L,
                new ArrayList<>()
        );
    }
}
