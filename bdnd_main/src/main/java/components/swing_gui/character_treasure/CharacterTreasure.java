package components.swing_gui.character_treasure;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Builder
@RequiredArgsConstructor
public class CharacterTreasure {
    private final CharacterMoney carriedTreasure;
    private final CharacterMoney atHomeTreasure;
    private final CharacterMoney elsewhereTreasure;

    public CharacterTreasure() {
        this(
                new CharacterMoney(),
                new CharacterMoney(),
                new CharacterMoney()
        );
    }
}
