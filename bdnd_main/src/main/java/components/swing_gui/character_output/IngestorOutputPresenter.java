package components.swing_gui.character_output;


import components.swing_gui.SwingController;
import lombok.Getter;

import javax.swing.*;

public class IngestorOutputPresenter implements IngestorOutputContract.Presenter {

  @Getter
  private final IngestorOutputView ingestorOutputView;

  private final SwingController swingController;

  public IngestorOutputPresenter(SwingController swingController) {
    this.swingController = swingController;

    ingestorOutputView = new IngestorOutputView();
  }

  @Override
  public void initialize() {
    configureView();

    addListeners();
  }

  private void configureView() {
    ingestorOutputView.initialize();
  }

  private void addListeners() {
    outputButtonListener();

    closeButtonListener();
  }

  private void outputButtonListener() {
    ingestorOutputView.getExportButton()
        .addActionListener(e -> {
          onOutput();
        });
  }

  @Override
  public void onOutput() {
    swingController.print();
  }

  private void closeButtonListener() {
    ingestorOutputView.getCloseButton()
        .addActionListener(e -> {
          onClose();
        });
  }


  @Override
  public void onClose() {
    SwingUtilities.getWindowAncestor(ingestorOutputView).dispose();
  }
}
