package components.swing_gui.character_output;

import contracts.gui_contract.GuiDto;

import javax.swing.*;

public interface IngestorOutputContract {
  interface Model {
    GuiDto get();

    void set(GuiDto guiDto);
  }

  interface Presenter {

    void initialize();

    void onOutput();

    void onClose();
  }

  interface View {
    void initialize();

    JButton getCloseButton();

    JButton getExportButton();
  }
}
