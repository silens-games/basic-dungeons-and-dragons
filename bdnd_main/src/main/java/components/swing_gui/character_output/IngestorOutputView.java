package components.swing_gui.character_output;

import lombok.Getter;

import javax.swing.*;
import java.awt.*;

public class IngestorOutputView extends JPanel implements IngestorOutputContract.View {

  private GridBagLayout gridBagLayout;

  private GridBagConstraints gridBagConstraints;

  @Getter
  private JButton closeButton;

  @Getter
  private JButton exportButton;

  public IngestorOutputView() {
    gridBagLayout = new GridBagLayout();
    gridBagConstraints = new GridBagConstraints();
    gridBagConstraints.insets = new Insets(5, 5, 5, 5);

    this.setLayout(gridBagLayout);
  }

  @Override
  public void initialize() {
    configureMainJPanel();

    addRowHorizontalSpaceLabel();

    addButton();

    cancelButton();
  }

  private void configureMainJPanel() {
    gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;

    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 0;
  }

  private void addRowHorizontalSpaceLabel() {
    gridBagConstraints.weightx = 1;

    JLabel label = new JLabel("");

    this.add(label, gridBagConstraints);

    gridBagConstraints.gridx++;
  }

  private void addButton() {
    gridBagConstraints.weightx = 0;

    exportButton = new JButton("Export");
    exportButton.setVisible(true);

    this.add(exportButton, gridBagConstraints);

    gridBagConstraints.gridx++;
  }

  private void cancelButton() {
    gridBagConstraints.weightx = 0;

    closeButton = new JButton("Close");
    closeButton.setVisible(true);

    this.add(closeButton, gridBagConstraints);
  }
}
