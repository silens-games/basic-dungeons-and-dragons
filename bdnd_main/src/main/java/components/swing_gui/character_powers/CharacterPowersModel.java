package components.swing_gui.character_powers;


import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@RequiredArgsConstructor
public class CharacterPowersModel {
    private final List<String> equipmentList;

    public CharacterPowersModel() {
        this(
                new ArrayList<>()
        );
    }
}
