package components.swing_gui.template_table;

import lombok.Getter;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class TableContextMenu extends JPopupMenu {

  @Getter
  private JMenuItem addRow;

  @Getter
  private JMenuItem deleteRow;

  @Getter
  private JMenuItem replicateRow;

  @Getter
  private JMenuItem clearTable;

  public TableContextMenu() {}

  public void initialize(){
    addRow = new JMenuItem("Add Row");
    deleteRow = new JMenuItem("Delete Row");
    replicateRow = new JMenuItem("Replicate Row");
    clearTable = new JMenuItem("Clear Table");

    this.add(addRow);
    this.add(deleteRow);
    this.add(replicateRow);
    this.add(clearTable);
  }

  public static void main(String[] args) {
    JFrame frame = new JFrame("JTable Right Click Menu");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setSize(400, 300);

    JTable table = new JTable(5, 5);
    JScrollPane scrollPane = new JScrollPane(table);

    // Create the popup menu
    JPopupMenu popupMenu = new JPopupMenu();
    JMenuItem deleteItem = new JMenuItem("Delete");
    JMenuItem editItem = new JMenuItem("Edit");

    popupMenu.add(editItem);
    popupMenu.add(deleteItem);

    // Add action listeners for menu items (for demo purposes)
    deleteItem.addActionListener(e -> JOptionPane.showMessageDialog(frame, "Delete option clicked!"));
    editItem.addActionListener(e -> JOptionPane.showMessageDialog(frame, "Edit option clicked!"));

    // Attach the popup menu to the table
    table.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseReleased(MouseEvent e) {
        if (e.isPopupTrigger()) {
          int row = table.rowAtPoint(e.getPoint());
          int column = table.columnAtPoint(e.getPoint());

          if (!table.isRowSelected(row)) {
            table.changeSelection(row, column, false, false);
          }

          popupMenu.show(e.getComponent(), e.getX(), e.getY());
        }
      }
    });

    frame.add(scrollPane);
    frame.setVisible(true);
  }
}
