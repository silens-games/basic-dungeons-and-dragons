//package components.swing_gui.template_table;
//
//import contracts.gui_contract.GuiDto;
//import logger.Log;
//import lombok.Getter;
//
//import javax.swing.*;
//import javax.swing.table.DefaultTableModel;
//import java.awt.event.MouseAdapter;
//import java.awt.event.MouseEvent;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Vector;
//import java.util.stream.IntStream;
//
//import components.swing_gui.template_table.renderers.TableRenderers;
//
//public class TablePresenter implements TableContract.Presenter {
//  private final Log logger = new Log();
//
//  @Getter
//  private final TableView tableView;
//
//  @Getter
//  private final SelectedRows selectedRows;
//
//  private HashMap<String, NetId> resultMap = new HashMap<String, NetId>();
//
//  private TableContextMenu tableContextMenu;
//
//  public TablePresenter() {
//    this.tableContextMenu = new TableContextMenu();
//    this.tableView = new TableView(new TableRenderers());
//    this.selectedRows = new SelectedRows();
//  }
//
//  protected TablePresenter(TableView tableView) {
//    this.tableView = tableView;
//
//    this.tableContextMenu = new TableContextMenu();
//    this.selectedRows = new SelectedRows();
//  }
//
//  @Override
//  public void initialize() {
//    configureView();
//
//    addListeners();
//  }
//
//  private void configureView() {
//    tableView.initialize();
//    tableContextMenu.initialize();
//  }
//
//  private void addListeners() {
//    addContextMenuListener();
//
//    addRowListener();
//
//    deleteRowListener();
//
//    replicateRowListener();
//
//    clearTableListener();
//  }
//
//  private void addContextMenuListener() {
//    JTable table = tableView.getTable();
//    table.addMouseListener(new MouseAdapter() {
//      @Override
//      public void mouseReleased(MouseEvent e) {
//        if (e.isPopupTrigger()) {
//          int row = table.rowAtPoint(e.getPoint());
//          int column = table.columnAtPoint(e.getPoint());
//
//          if (!table.isRowSelected(row)) {
//            table.changeSelection(row, column, false, false);
//          }
//
//          tableContextMenu.show(e.getComponent(), e.getX(), e.getY());
//        }
//      }
//    });
//  }
//
//  private void addRowListener() {
//    tableContextMenu.getAddRow().addActionListener(e -> {
//      onAddRow();
//    });
//  }
//
//  @Override
//  public void onAddRow() {
//    addRow(TableElementModel.builder()
//        .net("Net")
//        .id("")
//        .cueStartInMHz(0.0)
//        .cueStopInMHz(0.0)
//        .manStartInMHz(0.0)
//        .manStopInMHz(0.0)
//        .build());
//  }
//
//  private void deleteRowListener() {
//    tableContextMenu.getDeleteRow().addActionListener(e -> {
//      onDeleteRow();
//    });
//  }
//
//  @Override
//  public void onDeleteRow() {
//    int selectedRows[] = tableView.getTable().getSelectedRows();
//
//    for (int i = selectedRows.length - 1; i >= 0; i--) {
//      ((DefaultTableModel) tableView.getTable().getModel()).removeRow(selectedRows[i]);
//    }
//  }
//
//  private void replicateRowListener() {
//    tableContextMenu.getReplicateRow().addActionListener(e -> {
//      onReplicateRow();
//    });
//  }
//
//  @Override
//  public void onReplicateRow() {
//    DefaultTableModel model = (DefaultTableModel) tableView.getTable().getModel();
//    int[] selectedRows = tableView.getTable().getSelectedRows();
//
//    for (int row = 0; row < selectedRows.length; row++) {
//      Object[] rowData = new Object[model.getColumnCount()];
//
//      for (int col = 0; col < model.getColumnCount(); col++) {
//        rowData[col] = model.getValueAt(selectedRows[row], col);
//      }
//
//      rowData[0] = (String) rowData[0] + row;
//
//      model.addRow(rowData);
//    }
//  }
//
//  private void clearTableListener() {
//    tableContextMenu.getClearTable().addActionListener(e -> {
//      onClearTable();
//    });
//  }
//
//  @Override
//  public void onClearTable() {
//    if (tableView.getTable() != null
//        && tableView.getDefaultTableModel() != null) {
//      tableView.getDefaultTableModel().setRowCount(0);
//
//      tableView.getTable().removeAll();
//
//      selectedRows.clear();
//
//      resultMap.clear();
//    }
//  }
//
//  private synchronized void createRows(TableModel tableModel) {
//    for (TableElementModel tableElementModel : tableModel.getTableElementModelList()) {
//      addRow(tableElementModel);
//    }
//  }
//
//  public void addRow(TableElementModel tableElementModel) {
//    TableModel tableModel = getViewModel();
//
//    int index = IntStream.range(0, tableModel.getTableElementModelList().size())
//        .filter(i -> tableModel.getTableElementModelList().get(i).getNet().equalsIgnoreCase(tableElementModel.getNet()))
//        .findFirst()
//        .orElse(-1);
//
//    if (index == -1) {
//      addRow(tableElementModel);
//    } else {
//      modifyRow(tableElementModel);
//    }
//  }
//
//  private void modifyRow(TableElementModel tableElementModel) {
//    Vector<Object> row = resultMap.get(tableElementModel.getNet()).getRow();
//    row.removeAllElements();
//    row.add(tableElementModel.getNet()); // If ID is not first row added modify getIdColumnIndex()
//    row.add(tableElementModel.getId());
//    row.add(tableElementModel.getCueStartInMHz());
//    row.add(tableElementModel.getCueStopInMHz());
//    row.add(tableElementModel.getManStartInMHz());
//    row.add(tableElementModel.getManStopInMHz());
//
//    refreshTableWhilePreservingSelectedRows();
//  }
//
//  private void refreshTableWhilePreservingSelectedRows() {
//    List<TableElementModel> tableElementModelList = selectedRows.from(tableView.getTable());
//
//    tableView.getDefaultTableModel().fireTableDataChanged();
//
//    selectedRows.select(tableElementModelList, tableView.getTable());
//  }
//
//  private void addRow(TableElementModel tableElementModel) {
//    Vector<Object> row = new Vector<Object>();
//    row.add(tableElementModel.getNet()); // If ID is not first row added modify getIdColumnIndex()
//    row.add(tableElementModel.getId());
//    row.add(tableElementModel.getCueStartInMHz());
//    row.add(tableElementModel.getCueStopInMHz());
//    row.add(tableElementModel.getManStartInMHz());
//    row.add(tableElementModel.getCueStopInMHz());
//
//    if (tableView.getDefaultTableModel() != null) {
//      tableView.getDefaultTableModel().addRow(row);
//      NetId netId = new NetId(tableElementModel.getNet(), row);
//      resultMap.put(tableElementModel.getNet(), netId);
//      refreshTableWhilePreservingSelectedRows();
//    }
//  }
//
//  @Override
//  public void setView(GuiDto guiDto) {
//    setView(adapt(guiDto));
//  }
//
//  @Override
//  public void clearView() {
//    setView(new TableModel());
//  }
//
//  @Override
//  public GuiDto getModel() {
//    return adapt(getViewModel());
//  }
//
//  private void setView(TableModel tableModel) {
//    createRows(tableModel);
//  }
//
//  private TableModel getViewModel() {
//    List<TableElementModel> tableElementModelList = new ArrayList<>();
//
//    DefaultTableModel model = (DefaultTableModel) tableView.getTable().getModel();
//    int rowCount = model.getRowCount();
//    int columnCount = model.getColumnCount();
//
//    for (int row = 0; row < rowCount; row++) {
//      Object[] rowData = new Object[columnCount];
//      for (int column = 0; column < columnCount; column++) {
//        rowData[column] = model.getValueAt(row, column);
//      }
//
//      tableElementModelList.add(TableElementModel.builder()
//          .net((String) model.getValueAt(row, 0))
//          .id((String) model.getValueAt(row, 1))
//          .cueStartInMHz((double) model.getValueAt(row, 2))
//          .cueStopInMHz((double) model.getValueAt(row, 3))
//          .manStartInMHz((double) model.getValueAt(row, 4))
//          .manStopInMHz((double) model.getValueAt(row, 5))
//          .build());
//    }
//
//    return TableModel.builder()
//        .tableElementModelList(tableElementModelList)
//        .build();
//  }
//
//  private TableModel adapt(GuiDto guiDto) {
//    List<TableElementModel> tableElementModelList = new ArrayList<>();
//
//    for (GuiElementDto guiElementDto : guiDto.getGuiElementDtoList()) {
//      tableElementModelList.add(TableElementModel.builder()
//          .net(guiElementDto.getNet())
//          .id(guiElementDto.getId())
//          .cueStartInMHz(guiElementDto.getCueStartInMHz())
//          .cueStopInMHz(guiElementDto.getCueStopInMHz())
//          .manStartInMHz(guiElementDto.getManStartInMHz())
//          .manStopInMHz(guiElementDto.getManStopInMHz())
//          .build());
//    }
//
//    return TableModel.builder()
//        .tableElementModelList(tableElementModelList)
//        .build();
//  }
//
//  private GuiDto adapt(TableModel tableModel) {
//    List<GuiElementDto> GuiElementDtoList = new ArrayList<>();
//
//    for (TableElementModel tableElementModel : tableModel.getTableElementModelList()) {
//      GuiElementDtoList.add(GuiElementDto.builder()
//          .net(tableElementModel.getNet())
//          .id(tableElementModel.getId())
//          .cueStartInMHz(tableElementModel.getCueStartInMHz())
//          .cueStopInMHz(tableElementModel.getCueStopInMHz())
//          .manStartInMHz(tableElementModel.getManStartInMHz())
//          .manStopInMHz(tableElementModel.getManStopInMHz())
//          .build());
//    }
//
//    return GuiDto.builder()
//            .id(0)
//        .build();
//  }
//}
//
