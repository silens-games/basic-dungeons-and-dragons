package components.swing_gui.template_table;

import components.swing_gui.template_table.renderers.TableRenderers;
import logger.Log;
import lombok.Getter;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import colors.Marine;
import lombok.Setter;

public class TableView extends JPanel implements TableContract.View {
  private Log logger = new Log();

  private GridBagLayout gridBagLayout;

  private GridBagConstraints gridBagConstraints;

  private final List<TableColumnSettings> tableColumnSettingsList;

  @Getter
  @Setter
  private JTable table;

  @Getter
  @Setter
  private DefaultTableModel defaultTableModel;

  public TableView(TableRenderers tableRenderers) {
    gridBagLayout = new GridBagLayout();
    gridBagConstraints = new GridBagConstraints();
    gridBagConstraints.insets = new Insets(5, 5, 5, 5);

    tableColumnSettingsList = new ArrayList<>(Arrays.asList(
        new TableColumnSettings("Net", 150, String.class, tableRenderers.getStringFormatRenderer()),
        new TableColumnSettings("ID", 30, String.class, tableRenderers.getStringFormatRenderer()),
        new TableColumnSettings("Cue Start (MHz)", 100, Double.class, tableRenderers.getFrequencyFormatRenderer()),
        new TableColumnSettings("Cue Stop (MHz)", 100, Double.class, tableRenderers.getFrequencyFormatRenderer()),
        new TableColumnSettings("Man Start (MHz)", 100, Double.class, tableRenderers.getFrequencyFormatRenderer()),
        new TableColumnSettings("Man Stop (MHz)", 100, Double.class, tableRenderers.getFrequencyFormatRenderer())
    ));

    this.setLayout(gridBagLayout);
  }

  @Override
  public void initialize() {
    configureMainJPanel();

    addSignalsTable();
  }

  private void configureMainJPanel() {
    this.setBorder(new TitledBorder(BorderFactory.createLineBorder(Marine.GOLD), "Expected Signals", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.BOLD, 16), Marine.BLUE));

    gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;

    gridBagConstraints.gridy = 0;
  }

  private void addSignalsTable() {
    gridBagConstraints.gridx = 0;

    JScrollPane scrollPaneResponseTable = new JScrollPane(configureTable());
    scrollPaneResponseTable.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
    scrollPaneResponseTable.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

    this.add(scrollPaneResponseTable, gridBagConstraints);
  }

  private JTable configureTable() {
    setAlternateRowColor();

    defaultTableModel = addColumnNamesToTableModel(createTableModel());

    table.setModel(defaultTableModel);
    table.setAutoCreateRowSorter(true);
    table.setFillsViewportHeight(true);
    table.setRowSelectionAllowed(true);
    table.setColumnSelectionAllowed(false);
    table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    table.setForeground(Marine.BLUE);

    forceScrollableViewToTableWidth(true);

    setColumnWidths();

    formatTableColumns();

    table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

    return table;
  }

  private void setAlternateRowColor() {
    table = new JTable(new Object[][]{{"115", "Ramesh"}, {"120", "Adithya"}, {"125", "Jai"}, {"130", "Chaitanya"}, {"135", "Raja"}}, new String[]{"Net", "Id"}) {
      public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
        Component comp = super.prepareRenderer(renderer, row, column);

        Color alternateRowColor = Marine.YELLOW;
        if (!comp.getBackground().equals(getSelectionBackground())) {
          Color rowColor = (row % 2 == 0 ? Color.LIGHT_GRAY : alternateRowColor);
          comp.setBackground(rowColor);
        }

        return comp;
      }
    };
  }

  private DefaultTableModel createTableModel() {
    return new DefaultTableModel() {
      @SuppressWarnings({"unchecked", "rawtypes"})
      @Override
      public Class getColumnClass(int column) {
        return tableColumnSettingsList.get(column).getType();
      }

      @Override
      public boolean isCellEditable(int row, int column) {
        return true;
      }
    };
  }

  private DefaultTableModel addColumnNamesToTableModel(DefaultTableModel defaultTableModel) {
    Vector<String> columnsResponse = new Vector<String>();

    tableColumnSettingsList.forEach(tableColumnSettings -> {
      columnsResponse.add(tableColumnSettings.getName());
    });

    defaultTableModel.setColumnIdentifiers(columnsResponse);

    return defaultTableModel;
  }

  private void setColumnWidths() {
    for (int i = 0; i < tableColumnSettingsList.size(); i++) {
      table.getColumnModel().getColumn(i).setPreferredWidth(tableColumnSettingsList.get(i).getWidth());
    }
  }

  private void formatTableColumns() {
    for (int i = 0; i < tableColumnSettingsList.size(); i++) {
      table.getColumnModel().getColumn(i).setCellRenderer(tableColumnSettingsList.get(i).getRenderer());
    }
  }

  private void forceScrollableViewToTableWidth(boolean isForced) {
    if (isForced) {
      int tableDefaultWidth = tableColumnSettingsList.stream().mapToInt(o -> o.getWidth()).sum();
      table.setPreferredScrollableViewportSize(new Dimension(tableDefaultWidth, 580));
    }
  }
}
