package components.swing_gui.template_table;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Builder
@RequiredArgsConstructor
public class TableElementModel {
  private final String net;
  private final String id;
  private final double cueStartInMHz;
  private final double cueStopInMHz;
  private final double manStartInMHz;
  private final double manStopInMHz;

  public TableElementModel() {
    this(
        "",
        "",
        0.0,
        0.0,
        0.0,
        0.0
    );
  }
}
