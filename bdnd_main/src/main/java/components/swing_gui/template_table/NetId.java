package components.swing_gui.template_table;

import java.util.Vector;

public class NetId {

  Vector<Object> row;
  String net;
  private static int count = 0;

  public NetId(String net, Vector<Object> rowData) {
    row = rowData;
    this.net = net;
    count++;
  }

  public Vector<Object> getRow() {
    return row;
  }

  public void setRow(Vector<Object> row) {
    this.row = row;
  }

  public static int getCount() {
    count++;
    return count;
  }

  public static void setCount(int count) {
    NetId.count = count;
  }

}
