package components.swing_gui.template_table.renderers;

import lombok.Synchronized;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DateFormatRenderer extends DefaultTableCellRenderer {

  private static final SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH); // "dd-M-yyyy hh:mm:ss"

  private final List<Integer> nonCompliantRows = new ArrayList<>();

  @Override
  public Component getTableCellRendererComponent(
      JTable table,
      Object value,
      boolean isSelected,
      boolean hasFocus,
      int row,
      int column) {

    JLabel component = (JLabel) colorNonCompliantRowsRed(row, table, super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column));

    component.setText(formatter.format(value));

    this.setHorizontalAlignment(JLabel.CENTER);

    return component;
  }

  private Component colorNonCompliantRowsRed(int row, JTable table, Component component){
    if (nonCompliantRows.contains(row)) {
      component.setForeground(Color.RED);
    } else {
      component.setForeground(table.getForeground());
    }

    return component;
  }

  public void clearNonCompliantRows(){
    nonCompliantRows.clear();
  }

  @Synchronized
  public void addNonCompliantRow(int row){
    nonCompliantRows.add(row);
  }

  @Synchronized
  public void removeNonCompliantRow(int row){
    if (nonCompliantRows.contains(row)) {
      nonCompliantRows.remove(Integer.valueOf(row));
    }
  }

  public int[] getNonCompliantRows(){
    return nonCompliantRows.stream().mapToInt(Integer::intValue).toArray();
  }
}