package components.swing_gui.template_table.renderers;

import colors.Marine;
import logger.Log;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.text.DecimalFormat;

public class FrequencyFormatRenderer extends DefaultTableCellRenderer {
  Log logger = new Log();

  private static final double MIN_FREQUENCY = 5_000.0;
  private static final double MAX_FREQUENCY = 6_000_000_000.0;

  private static final DecimalFormat formatter = new DecimalFormat("#.000000");

  public Component getTableCellRendererComponent(
      JTable table, Object value, boolean isSelected,
      boolean hasFocus, int row, int column) {

    double frequency = 0.0;
    if (value instanceof Double) {
      frequency = (double) value;
      value = formatter.format(frequency);
    }

    Component component = colorNonCompliantRowsRed(frequency * 1_000_000.0, table, super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column));

    this.setHorizontalAlignment(JLabel.CENTER);

    return component;
  }

  private Component colorNonCompliantRowsRed(double frequencyInHz, JTable table, Component component) {
    if (frequencyInHz < MIN_FREQUENCY || MAX_FREQUENCY < frequencyInHz) {
      component.setForeground(Marine.RED);
    } else {
      component.setForeground(table.getForeground());
    }

    return component;
  }
}
