package components.swing_gui.template_table.renderers;

import lombok.Getter;

@Getter
public class TableRenderers {

  private final StringFormatRenderer stringFormatRenderer;
  private final IntegerFormatRenderer integerFormatRenderer;
  private final DecimalFormatRenderer decimalFormatRenderer;
  private final DateFormatRenderer dateFormatRenderer;
  private final FrequencyFormatRenderer frequencyFormatRenderer;

  public TableRenderers() {
    this.stringFormatRenderer = new StringFormatRenderer();
    this.integerFormatRenderer = new IntegerFormatRenderer();
    this.decimalFormatRenderer = new DecimalFormatRenderer();
    this.dateFormatRenderer = new DateFormatRenderer();
    this.frequencyFormatRenderer = new FrequencyFormatRenderer();
  }
}
