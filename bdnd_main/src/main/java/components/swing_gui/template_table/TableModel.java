package components.swing_gui.template_table;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@RequiredArgsConstructor
public class TableModel implements TableContract.Model {

  private final List<TableElementModel> tableElementModelList;

  public TableModel() {
    this(new ArrayList<>());
  }

  public TableModel getTableData() {
    return this;
  }

  public void setTableData(TableModel tableModel) {
    tableElementModelList.clear();

    for (TableElementModel tableElementModel : tableModel.getTableElementModelList()) {
      tableElementModelList.add(TableElementModel.builder()
          .net(tableElementModel.getNet())
          .id(tableElementModel.getId())
          .cueStartInMHz(tableElementModel.getCueStartInMHz())
          .cueStopInMHz(tableElementModel.getCueStopInMHz())
          .manStartInMHz(tableElementModel.getManStopInMHz())
          .manStopInMHz(tableElementModel.getManStopInMHz())
          .build());
    }
  }
}
