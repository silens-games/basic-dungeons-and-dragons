package components.swing_gui.template_table;

import javax.swing.*;
import contracts.gui_contract.*;

public interface TableContract {
  interface Model {
    TableModel getTableData();

    void setTableData(TableModel tableModel);
  }

  interface Presenter {
    void initialize();

    void onAddRow();

    void onDeleteRow();

    void onReplicateRow();

    void onClearTable();

    GuiDto getModel();

    void clearView();

    void setView(GuiDto GuiDto);
  }

  interface View {
    void initialize();

    JTable getTable();
  }
}
