package components.swing_gui.template_table;

import logger.Log;
import lombok.Setter;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SelectedRows {
  private Log logger = new Log();

  @Setter
  private Map<String, TableElementModel> rowMap;

  public SelectedRows() {
    rowMap = new HashMap<>();
  }

  public List<TableElementModel> from(JTable table) {
    DefaultTableModel defaultTableModel = (DefaultTableModel) table.getModel();

    List<TableElementModel> tableElementModelList = new ArrayList<>();

    int listSelectedRowIndexes[];

    if (defaultTableModel != null
        && table.getRowCount() > 0
        && table.getSelectedRowCount() > 0) {

      listSelectedRowIndexes = table.getSelectedRows();

      for (int index : listSelectedRowIndexes) {
        String selectedNet = table.getValueAt(index, 0).toString();

        for (Map.Entry<String, TableElementModel> entry : rowMap.entrySet()) {
          if (entry.getValue().getNet().equalsIgnoreCase(selectedNet)) {
            tableElementModelList.add(entry.getValue());
            break;
          }
        }
      }
    }

    return tableElementModelList;
  }

  public void select(List<TableElementModel> tableElementModelList, JTable table) {
    for (TableElementModel tableElementModel : tableElementModelList) {
      for (int rowIndex = 0; rowIndex < table.getRowCount(); rowIndex++) {
        String rowNet = table.getValueAt(rowIndex, 0).toString();

        ListSelectionModel listSelectionModel = table.getSelectionModel();

        if (tableElementModel.getNet().equalsIgnoreCase(rowNet)) {
          listSelectionModel.addSelectionInterval(rowIndex, rowIndex);
          break;
        }
      }
    }
  }

  public void setSignalList(TableModel tableModel) {
    for (TableElementModel tableElementModel : tableModel.getTableElementModelList()) {
      rowMap.put(tableElementModel.getNet(), tableElementModel);
    }
  }

  public void clear() {
    rowMap.clear();
  }
}
