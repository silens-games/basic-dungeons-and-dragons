package components.swing_gui.template_table;


import components.swing_gui.template_table.renderers.StringFormatRenderer;
import lombok.*;

import javax.swing.table.TableCellRenderer;

@Getter
@Builder
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor
public class TableColumnSettings {
  private final String name;

  private final Integer width;

  private final Class type;

  private final TableCellRenderer renderer;

  public TableColumnSettings() {
    this(
        "",
        0,
        String.class,
        new StringFormatRenderer()
    );
  }
}
