package components.swing_gui.character_abilities;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class CharacterAbilitiesModel {
    private final int strength;
    private final int intelligence;
    private final int dexterity;
    private final int wisdom;
    private final int constitution;
    private final int charisma;

    private final int strengthAdjustment;
    private final int intelligenceAdjustment;
    private final int dexterityAdjustment;
    private final int wisdomAdjustment;
    private final int constitutionAdjustment;
    private final int charismaAdjustment;

    public CharacterAbilitiesModel() {
        this(
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0);
    }
}
