package components.swing_gui.character_experience;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data@RequiredArgsConstructor
public class CharacterExperienceModel {
    private final String primeRequirement;
    private final int xpAdjustment;
    private final long xpTotal;

    public CharacterExperienceModel() {
        this(
                "",
                0,
                0L
        );
    }
}
