package components.csv_output;

import contracts.export_contract.ExportDto;
import contracts.export_contract.Exporter;

import java.io.*;
import java.nio.file.Path;

public class CsvWriter implements Exporter {

  private static final String CSV_SEPARATOR = ",";

  @Override
  public void write(Path csvPath, ExportDto exportDto) {
    try {
      try (BufferedWriter writer = new BufferedWriter(new FileWriter(csvPath.toString(), true))) {
          writer.write(
                  exportDto.getId() + CSV_SEPARATOR +
                      exportDto.getId() + CSV_SEPARATOR +
                      exportDto.getId() + CSV_SEPARATOR +
                      exportDto.getId() + CSV_SEPARATOR +
                      exportDto.getId());
          writer.newLine();
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static ExportDto read(Path csvPath) throws IOException {
    ExportDto exportDto = new ExportDto();

    try (BufferedReader reader = new BufferedReader(new FileReader(csvPath.toString()))) {
      String line;
      while ((line = reader.readLine()) != null) {
        String[] data = line.split(CSV_SEPARATOR);

        if (data.length == 4) {
//          exportDto.getExportElementDtoList().add(exportElementDto.builder()
//                  .net(data[0])
//                  .id(data[1])
//                  .cueStartInMHz(Double.parseDouble(data[2]))
//                  .cueStopInMHz(Double.parseDouble(data[3]))
//                  .manStartInMHz(Double.parseDouble(data[4]))
//                  .manStopInMHz(Double.parseDouble(data[5]))
//              .build());
        }
      }
    }

    return exportDto;
  }
}