package controller;

import file.TextFile;
import logger.Log;

import javax.swing.*;

import contracts.gui_contract.Gui;

public class Main {
  private static Log logger = new Log();

  public static void main(String[] args) {
    logger.info("Starting Template.");

    setLookAndFeel();

    DependencyInjector dependencyInjector = new DependencyInjector();

    Gui gui = dependencyInjector.getGui();
    gui.initialize();
  }

  private static void setLookAndFeel() {
    try {
      UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
    } catch (UnsupportedLookAndFeelException | IllegalAccessException | InstantiationException |
             ClassNotFoundException e) {
      logger.error("error setting Look and Feel to Windows.", e);
    }
  }
}
