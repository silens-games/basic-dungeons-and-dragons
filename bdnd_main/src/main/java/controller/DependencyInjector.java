package controller;

import components.swing_gui.SwingController;
import contracts.export_contract.Exporter;
import contracts.gui_contract.Gui;

public class DependencyInjector {

  private Gui gui;

  private Exporter exporter;


  public DependencyInjector() {
  }

  public Gui getGui() {
    if (gui == null) {
      gui = new SwingController();
    }
    return gui;
  }
//
//  public Exporter getExporter() {
//    if (exporter == null) {
//      exporter = new CsvWriter();
//    }
//    return exporter;
//  }
}
