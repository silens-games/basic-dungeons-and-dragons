package controller;

import file.TextFile;
import logger.Log;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class BDnDBatchFile {
  private static Log logger = new Log();

  public static void main(String[] args) {
    logger.info("Created Template batch file.");

    createTemplateBatFile();
  }

  public static void createTemplateBatFile() {
    Path parentPath = Paths.get(System.getProperty("user.home") + File.separator +
        "AppData" + File.separator +
        "Local" + File.separator +
        "template" + File.separator +
        "executable");

    Path filePath = Paths.get(parentPath + File.separator + "template.bat");

    if (filePath.toFile().exists()) {
      // Do Nothing
    } else {
      parentPath.toFile().mkdirs();

      String batch = "start cmd /c \"java -jar %~dp0template_main.jar %*\" \"cmd /c exit\"";

      try {
        TextFile.write(filePath, batch);
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }
  }
}
