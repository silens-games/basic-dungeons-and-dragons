package contracts.gui_contract;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class GuiDto {

  private int id;

  public GuiDto() {
    this(0);
  }
}