package contracts.gui_contract;

public interface Gui {
  void initialize();
  void clearView();
  void setView(GuiDto guiDto);
  GuiDto getModel();
}
