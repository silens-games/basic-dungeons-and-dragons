package contracts.entity_contract;

import java.nio.file.Path;

public interface Entity {
  EntityDto getEntity(Path path);
}
