package contracts.entity_contract;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class EntityDto {

  private int id;

  public EntityDto() {
    this(0);
  }
}