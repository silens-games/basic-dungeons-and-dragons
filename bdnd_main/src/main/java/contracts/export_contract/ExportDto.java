package contracts.export_contract;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ExportDto {

  private int id;

  public ExportDto() {
    this(0);
  }
}