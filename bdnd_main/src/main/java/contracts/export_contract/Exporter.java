package contracts.export_contract;

import java.nio.file.Path;

public interface Exporter {
  void write(Path path, ExportDto exportDto);
}
