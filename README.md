# User Installation:
1. Install Java 17
2. 

# Current Releases:
### template

### COTS
- Java JRE 17

# Developer Installation

## Build

- Use gradle install method when building a project. This will auto deploy in the other subprojects that need jar.
- Running gradle install 

## Setup

1. Install Java 17


2. (**Intellij only**) Install Gradle 8.3 


3. Install Git For Windows


6. Install Intellij
    - set tab indents to 2 spaces
    - set table continuation indent to 4 spaces.


7. (**Intellij only**) Make sure the following plugins are install 
   - File → Settings → Plugins→ Marketplace → Lombok
   - File → Settings → Plugins → Marketplace→ Gradle
   - File → Settings → Plugins→ Marketplace → Gradle Extension
   - File → Settings → Plugins→ Marketplace → Maven
   - File → Settings → Plugins→ Marketplace → Maven Extension


8. Create environment variables
   - JAVA_HOME to java directory for example C:\Program Files\Java\<your java jdk>
   - (**Intellij only**) GRADLE_HOME to Gradle directory for example C:\Gradle\<your gradle>

   ### Add these to Path:
   - Java bin for example C:\Program Files\Java\<your java jdk>\bin
   - (**Intellij only**) Gradle bin for example C:\Gradle\<your gradle>\bin


9. **Optional** Software
   - Install BareTail (https://www.baremetalsoft.com/baretail/)
   - Install DB Browser for SQLite (https://sqlitebrowser.org/)
   - Install Gimp 2 (https://www.gimp.org/downloads/)

# Create .bat file

To create a batch file to run a Java 17 JAR executable, follow these steps:

1. Open a text editor, such as Notepad.

2. Copy and paste the following code into the text editor:

```bat
@echo off

REM Set the JAVA_HOME variable to the path of your Java 17 installation.
set JAVA_HOME=C:\Program Files\Java\jdk-17.0.5

REM Run the Java 17 JAR executable.
"C:\Program Files\Java\jdk-17.0.5\bin\java.exe" -jar "C:\Path\To\Your\JAR\Executable.jar"
```

3. Replace the C:\Program Files\Java\jdk-17.0.5 path with the actual path to your Java 17 installation.

4. Replace the C:\Path\To\Your\JAR\Executable.jar path with the actual path to your Java 17 JAR executable.

5. Save the file with a .bat extension, such as run_jar.bat.

6. Double-click the batch file to run the Java 17 JAR executable.

## Example

```bat
@echo off

REM Set the JAVA_HOME variable to the path of your Java 17 installation.
set JAVA_HOME=C:\Program Files\Java\jdk-17.0.5

REM Run the Java 17 JAR executable.
"C:\Program Files\Java\jdk-17.0.5\bin\java.exe" -jar "C:\Path\To\Your\JAR\Executable.jar"

```

This batch file will set the JAVA_HOME variable to the path of your Java 17 installation and then run the Java 17 JAR executable.

If you want the batch file to keep the console window open after running the JAR executable, you can remove the @echo off line at the beginning of the file.

You can also add additional parameters to the java.exe command to control how the JAR executable is run. For example, you can use the -Xmx parameter to specify the maximum amount of memory that the JAR executable can use.