package dice;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UnmodifiedRollOpenEndedTest {
    @Test
    @DisplayName("Testing Open Critical")
    @RepeatedTest(100)
    public void rollD100OpenCritical(){
        // Given
        UnmodifiedRoll initialRoll = UnmodifiedRoll.builder()
                .unmodifiedRoll(96)
                .build();

        // When
        int totalRoll = initialRoll.open().mod(17);

        // Then
        assertTrue((97 + 17) <= totalRoll);
    }

    @Test
    @DisplayName("Testing Open Fumble")
    @RepeatedTest(100)
    public void rollD100OpenFumble(){
        // Given
        UnmodifiedRoll initialRoll = UnmodifiedRoll.builder()
                .unmodifiedRoll(5)
                .build();

        // When
        int totalRoll = initialRoll.open().mod(17);

        // Then
        assertTrue(totalRoll <= (4 + 17));
    }

    @Test
    @DisplayName("Testing Open Fumble")
    public void rollD100Open95(){
        // Given
        UnmodifiedRoll initialRoll = UnmodifiedRoll.builder()
                .unmodifiedRoll(95)
                .build();

        // When
        int totalRoll = initialRoll.open().mod(17);

        // Then
        assertTrue(totalRoll == (95 + 17));
    }

    @Test
    @DisplayName("Testing Open Fumble")
    public void rollD100Open6(){
        // Given
        UnmodifiedRoll initialRoll = UnmodifiedRoll.builder()
                .unmodifiedRoll(6)
                .build();

        // When
        int totalRoll = initialRoll.open().mod(17);

        // Then
        assertTrue(totalRoll == (6 + 17));
    }
}