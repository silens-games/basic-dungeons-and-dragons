package dice;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;

class D4Test {

    private D4 d4;

    @BeforeEach
    void setUp() {
        d4 = new D4();
    }

    @RepeatedTest(value = 10)
    void should_RollBetweenOneAndFour_When_OneDieIsRolled() {
        // Given

        // When
        int actual = d4.roll();

        // Then
        assertAll(
                () -> assertTrue(actual >= 1),
                () -> assertTrue(actual <= 4)
        );
    }
}