package dice;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;

class D12Test {

    private D12 d12;

    @BeforeEach
    void setUp() {
        d12 = new D12();
    }

    @RepeatedTest(value = 10)
    void should_RollBetweenOneAndFour_When_OneDieIsRolled() {
        // Given

        // When
        int actual = d12.roll();

        // Then
        assertAll(
                () -> assertTrue(actual >= 1),
                () -> assertTrue(actual <= 12)
        );
    }
}