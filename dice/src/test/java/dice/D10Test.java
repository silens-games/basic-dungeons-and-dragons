package dice;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;

class D10Test {

    private D10 d10;

    @BeforeEach
    void setUp() {
        d10 = new D10();
    }

    @RepeatedTest(value = 10)
    void should_RollBetweenOneAndTen_When_OneDieIsRolled() {
        // Given

        // When
        int actual = d10.roll();

        // Then
        assertAll(
                () -> assertTrue(actual >= 1),
                () -> assertTrue(actual <= 10)
        );
    }
}