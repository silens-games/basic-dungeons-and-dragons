package dice;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;

class D20Test {

    private D20 d20;

    @BeforeEach
    void setUp() {
        d20 = new D20();
    }

    @RepeatedTest(value = 10)
    void should_RollBetweenOneAndTwenty_When_OneDieIsRolled() {
        // Given

        // When
        int actual = d20.roll();

        // Then
        assertAll(
                () -> assertTrue(actual >= 1),
                () -> assertTrue(actual <= 20)
        );
    }
}