package dice;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DiceFunctionalTest {


    @BeforeEach
    public void beforeEach() {

    }

    @Test
    @DisplayName("Testing 1d3+5")
    @RepeatedTest(10)
    public void testing1d3Plus5() {
        // Given

        // When
        int totalRoll = Dice.roll(1).d3().mod(5);

        // Then
        assertAll(
                () -> assertTrue(totalRoll >= 6),
                () -> assertTrue(totalRoll <= 8)
        );
    }

    @Test
    @DisplayName("Testing 2d10+7")
    @RepeatedTest(100)
    public void testing1d10Plus5() {
        // Given

        // When
        int totalRoll = Dice.roll(2).d10().mod(7);

        // Then
        assertAll(
                () -> assertTrue((2 + 7) <= totalRoll),
                () -> assertTrue(totalRoll <= (2 * 10 + 7))
        );
    }

    @Test
    @DisplayName("Testing 7d3+17")
    @RepeatedTest(100)
    public void testing7d3Plus17() {
        // Given

        // When
        int totalRoll = Dice.roll(7).d3().mod(17);

        // Then
        assertAll(
                () -> assertTrue((7 + 17) <= totalRoll),
                () -> assertTrue(totalRoll <= (7 * 3 + 17))
        );
    }
}