package dice;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;

class D8Test {

    private D8 d8;

    @BeforeEach
    void setUp() {
        d8 = new D8();
    }

    @RepeatedTest(value = 10)
    void should_RollBetweenOneAndEight_When_OneDieIsRolled() {
        // Given

        // When
        int actual = d8.roll();

        // Then
        assertAll(
                () -> assertTrue(actual >= 1),
                () -> assertTrue(actual <= 8)
        );
    }
}