package dice;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;

class D6Test {

    private D6 d6;

    @BeforeEach
    void setUp() {
        d6 = new D6();
    }

    @RepeatedTest(value = 10)
    void should_RollBetweenOneAndSix_When_OneDieIsRolled(){
        // Given

        // When
        int actual = d6.roll();

        // Then
        assertAll(
                () -> assertTrue(actual >= 1),
                () -> assertTrue(actual <= 6)
        );
    }
}