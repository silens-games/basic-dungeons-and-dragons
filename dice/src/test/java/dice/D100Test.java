package dice;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;

class D100Test {

    private D100 d100;

    @BeforeEach
    void setUp() {
        d100 = new D100();
    }

    @RepeatedTest(value = 10)
    void should_RollBetweenOneAndOneHundred_When_OneDieIsRolled() {
        // Given

        // When
        int actual = d100.roll();

        // Then
        assertAll(
                () -> assertTrue(actual >= 1),
                () -> assertTrue(actual <= 100)
        );
    }
}