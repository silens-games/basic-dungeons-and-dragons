package dice;

public class D8 extends Dice {
    @Override
    protected int roll() {
        return rand.nextInt(8) + 1;
    }
}
