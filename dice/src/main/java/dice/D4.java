package dice;

public class D4 extends Dice{
    @Override
    protected int roll(){
        return rand.nextInt(4) + 1;
    }

}
