package dice;

public class D10 extends Dice{
    @Override
    protected int roll(){
        return rand.nextInt(10) + 1;
    }
}
