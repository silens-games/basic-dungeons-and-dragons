package dice;

public class D20 extends Dice{
    @Override
    protected int roll(){
        return rand.nextInt(20) + 1;
    }
}
