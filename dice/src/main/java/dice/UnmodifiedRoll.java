package dice;

import dice.rolemaster.OpenEndedRoll;
import lombok.Builder;

@Builder
public class UnmodifiedRoll {
    private int unmodifiedRoll = 0;

    public int mod(int plus) {
        return unmodifiedRoll + plus;
    }

    public UnmodifiedRoll open() {
       return UnmodifiedRoll.builder()
               .unmodifiedRoll(OpenEndedRoll.roll(unmodifiedRoll))
               .build();
    }
}
