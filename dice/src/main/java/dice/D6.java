package dice;

public class D6 extends Dice{
    @Override
    protected int roll(){
        return rand.nextInt(6) + 1;
    }
}
