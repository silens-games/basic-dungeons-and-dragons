package dice;

import lombok.Builder;

@Builder
public class Die {
    private int numberOfDice = 1;

    public UnmodifiedRoll d3(){
        return UnmodifiedRoll.builder()
                .unmodifiedRoll(roll(new D3()))
                .build();
    }

    public UnmodifiedRoll d4(){
        return UnmodifiedRoll.builder()
                .unmodifiedRoll(roll(new D4()))
                .build();
    }

    public UnmodifiedRoll d6(){
        return UnmodifiedRoll.builder()
                .unmodifiedRoll(roll(new D6()))
                .build();
    }

    public UnmodifiedRoll d8(){
        return UnmodifiedRoll.builder()
                .unmodifiedRoll(roll(new D8()))
                .build();
    }

    public UnmodifiedRoll d10(){
        return UnmodifiedRoll.builder()
                .unmodifiedRoll(roll(new D10()))
                .build();
    }

    public UnmodifiedRoll d12(){
        return UnmodifiedRoll.builder()
                .unmodifiedRoll(roll(new D12()))
                .build();
    }

    public UnmodifiedRoll d20(){
        return UnmodifiedRoll.builder()
                .unmodifiedRoll(roll(new D20()))
                .build();
    }

    public UnmodifiedRoll d100(){
        return UnmodifiedRoll.builder()
                .unmodifiedRoll(roll(new D100()))
                .build();
    }

    public int roll(Dice dice){
        int total = 0;

        for(int r = 0; r < numberOfDice; r++){
            total += dice.roll();
        }

        return total;
    }
}
