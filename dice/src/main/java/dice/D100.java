package dice;

public class D100 extends Dice{
    @Override
    protected int roll(){
        return rand.nextInt(100) + 1;
    }
}
