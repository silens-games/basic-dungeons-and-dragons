package dice.rolemaster;

import dice.D100;
import dice.Dice;
import dice.UnmodifiedRoll;

public class OpenEndedRoll {

    public static int roll(int initialRoll) {
        if (isFumble(initialRoll)) {
            return getFumble(initialRoll);
        } else if (isCritical(initialRoll)) {
            return getCritical(initialRoll);
        } else {
            return initialRoll;
        }
    }

    private static boolean isFumble(int total) {
        return (total <= 5);
    }

    private static boolean isCritical(int total) {
        return (total >= 96);
    }

    private static int getFumble(int total) {
        int fumbleRoll = 0;

        do {
           fumbleRoll = Dice.roll(1, new D100(), 0);
            total -= fumbleRoll;
        } while (isCritical(fumbleRoll));

        return total;
    }

    private static int getCritical(int total) {
        int criticaleRoll = 0;

        do {
            criticaleRoll = Dice.roll(1).d100().mod(0);
            total += criticaleRoll;
        } while (isCritical(criticaleRoll));

        return total;
    }
}
