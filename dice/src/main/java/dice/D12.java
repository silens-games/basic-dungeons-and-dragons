package dice;

public class D12 extends Dice{
    @Override
    protected int roll(){
        return rand.nextInt(12) + 1;
    }
}
