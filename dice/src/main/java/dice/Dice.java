package dice;

import java.util.Random;

public abstract class Dice {
    protected static final Random rand = new Random(System.nanoTime());

    protected abstract int roll();

    public static int roll(int numberOfDice, Dice dice, int modification){
        int total = 0;

        for(int roll = 0; roll < numberOfDice; roll++){
            total = total + dice.roll();
        }

        return total + modification;
    }

    public static Die roll(int numberOfDice){
        return Die.builder()
                .numberOfDice(numberOfDice)
                .build();
    }
}
