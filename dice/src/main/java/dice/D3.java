package dice;

public class D3 extends Dice {
    @Override
    protected int roll() {
        return rand.nextInt(3) + 1;
    }
}
