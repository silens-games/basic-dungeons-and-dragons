package object_storage;

import logger.Log;
import object_storage.exceptions.JsonFileFailedToCreateDirectoryException;
import object_storage.exceptions.JsonFileFailedToDeleteFileException;
import object_storage.exceptions.JsonFileFailedToMakeFileWritableException;

import java.io.File;
import java.io.IOException;

public abstract class JsonFile<T> {
  private final Log logger = new Log();

  protected final String FILE_PATH = System.getProperty("user.home") + "/AppData/Local/sgp/";

  protected final String fileName;

  protected final Class<T> classType;

  protected final File file;

  public JsonFile(final Class<T> classType, final String fileName) {
    this.fileName = fileName;
    this.classType = classType;
    this.file = new File(FILE_PATH + fileName);

    createNewFileIfItDoesNotExist();
  }

  private void createNewFileIfItDoesNotExist() {
    if (file.exists()) {
      //Do nothing
    } else {
      createDirectory();
      createFile();
      makeFileWritable();
    }
  }

  private void createDirectory() {
    try {
      tryToCreateDirectory();
    } catch (JsonFileFailedToCreateDirectoryException e) {
      logger.error(String.format("\n\nJsonFile.createDirectory() failed to create %s", file.getAbsolutePath()), e);
    }
  }

  private void tryToCreateDirectory() throws JsonFileFailedToCreateDirectoryException {
    boolean successfullyCreatedDirectory = false;
    if (file.getParentFile().exists()) {
      successfullyCreatedDirectory = true;
    }else{
      successfullyCreatedDirectory = file.getParentFile().mkdirs();
    }

    if (successfullyCreatedDirectory) {
      //Do nothing
    } else {
      throw new JsonFileFailedToCreateDirectoryException("JsonFile.createDirectory() failed to create the file directory. " + file.getParentFile());
    }
  }

  private void createFile() {
    try {
      file.createNewFile();
    } catch (IOException e) {
    }
  }

  private void makeFileWritable() {
    try {
      tryToMakeFileWritable();
    } catch (JsonFileFailedToMakeFileWritableException e) {
      logger.error(String.format("\n\nJsonFile.createFile() User does not have permission to write to %s", file.getAbsoluteFile()), e);
    }
  }

  private void tryToMakeFileWritable() throws JsonFileFailedToMakeFileWritableException {
    boolean successfullyMadeFileWritable = file.setWritable(true);

    if (successfullyMadeFileWritable) {
      //Do nothing
    } else {
      throw new JsonFileFailedToMakeFileWritableException("JsonFile.makeFileWritable() failed to make file writable, because the user does not have permission to change the access permissions of this abstract pathname.");
    }
  }

  public void createNewJsonFile() {
    deleteJsonFile();
    createNewFileIfItDoesNotExist();
  }

  private void deleteJsonFile() {
    try {
      tryToDeleteFile();
    } catch (JsonFileFailedToDeleteFileException e) {
      logger.error(String.format("\n\nJsonFile.deleteJsonFile() failed to delete %s", file.getAbsoluteFile()), e);
    }
  }

  private void tryToDeleteFile() throws JsonFileFailedToDeleteFileException {
    if (file.exists()) {
      boolean successfullyDeletedFile = file.delete();

      if (successfullyDeletedFile) {
        //Do nothing
      } else {
        throw new JsonFileFailedToDeleteFileException("JsonFile.tryToDeleteFile() failed to delete.");
      }
    }
  }
}
