package colors;

import java.awt.Color;

public class Marine {
  public final static Color GOLD = new Color(0xA7, 0x7C, 0x29);
  public final static Color BLUE = new Color(0x00, 0x44, 0x81);
  public final static Color RED = new Color(0xCC, 0x10, 0x1F);
  public final static Color YELLOW = new Color(0xFF, 0xD5, 0x00);
  public final static Color GREY = new Color(0x75, 0x75, 0x75);
}
