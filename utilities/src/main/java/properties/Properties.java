package properties;

import java.util.Map;

public interface Properties<T> {
  void save(Map<String, T> properties);
  Map<String, T> load(Class<T> clazz);
}
