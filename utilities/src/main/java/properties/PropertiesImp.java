package properties;

import file.FileCreator;
import serializer.Serializer;
import logger.Log;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

public class PropertiesImp<T> implements Properties<T>{
  private Log logger = new Log(this.getClass());

  private java.util.Properties utilProperties;

  private final Path propertiesPath;

  public PropertiesImp(Path propertiesPath) {
    this.utilProperties = new java.util.Properties();

    this.propertiesPath = propertiesPath;

    FileCreator.createIfNotExists(propertiesPath);
  }

  public void save(Map<String, T> properties) {
    properties.forEach((k, v) -> {
      utilProperties.setProperty(k, Serializer.toJson(v));
    });

    FileOutputStream outputStream = getOutputStream();

    storeProperties(outputStream);

    closeOutputStream(outputStream);
  }

  private FileOutputStream getOutputStream() {
    FileOutputStream outputStream = null;
    try {
      outputStream = new FileOutputStream(propertiesPath.toString());
    } catch (FileNotFoundException e) {
      logger.info("Did not save properties.", e);
    }

    return outputStream;
  }

  private void storeProperties(FileOutputStream outputStream) {
    try {
      utilProperties.store(outputStream, "");
    } catch (IOException e) {
      logger.info("Did not save properties.", e);
    }
  }

  private void closeOutputStream(FileOutputStream outputStream) {
    try {
      outputStream.close();
    } catch (IOException e) {
      logger.info("Did not save properties.", e);
    }
  }

  public Map<String, T> load(Class<T> clazz) {

    loadProperties();

    Map<String, T> propertiesMap = new HashMap<>();

    for (String key : utilProperties.stringPropertyNames()) {
      propertiesMap.put(key, Serializer.fromJson(utilProperties.getProperty(key), clazz));
    }

    return propertiesMap;
  }

  private void loadProperties() {
    FileInputStream fileInputStream = null;
    try {
      fileInputStream = new FileInputStream(propertiesPath.toString());
      utilProperties.load(fileInputStream);
    } catch (IOException e) {
      logger.info("Did not load properties.", e);
    }

    if (fileInputStream != null) {
      closeInputStream(fileInputStream);
    }
  }

  private void closeInputStream(FileInputStream fileInputStream){
    try {
      fileInputStream.close();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
