package logger;

import com.google.gson.Gson;
import java.lang.invoke.MethodHandles;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Log {
  Logger logger;

  public Log(){
    this(MethodHandles.lookup().lookupClass());
  }

  public Log(Class clazz) {
    logger = LogManager.getLogger(clazz);
  }

  public void error(Object message){
    logger.error(message);
  }

  public void error(Throwable t){
    logger.error(t);
  }

  public void error(Object message, Throwable t){
    logger.error(message, t);
  }

  public void warn(Object message){
    logger.warn(message);
  }

  public void warn(Throwable t){
    logger.warn(t);
  }

  public void warn(Object message, Throwable t){
    logger.warn(message, t);
  }

  public void warnGson(Object obj){
    Gson gson =  new Gson();

    logger.warn(gson.toJson(obj));
  }

  public void info(Object message){
    logger.info(message);
  }

  public void info(Throwable t){
    logger.info(t);
  }

  public void info(Object message, Throwable t){
    logger.info(message, t);
  }
}
