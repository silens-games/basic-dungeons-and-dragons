package file;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class TextFile {
  public static void write(Path filePath, String text) throws
      IllegalArgumentException,
      IOException,
      UnsupportedOperationException,
      SecurityException  {

    Files.writeString(filePath, text, StandardCharsets.UTF_8);
  }

  public static String read(Path filePath) throws
      IOException,
      OutOfMemoryError,
      SecurityException {

    return Files.readString(filePath, StandardCharsets.UTF_8);
  }
}
