package file.file_chooser;

import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;

public interface FileChooser {
  File save(String defaultFileName);

  File save(String defaultFileName, FileNameExtensionFilter filter);

  File open(String defaultFileName);

  File open(String defaultFileName, FileNameExtensionFilter filter);

  File getFile();
}
