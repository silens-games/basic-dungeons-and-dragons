package file.file_chooser;

import logger.Log;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import java.io.IOException;

public class FileChooserImp implements FileChooser {
  private Log logger = new Log();

  private File file;

  private File parentPath;

  private enum ChooserType {
    SAVE,
    OPEN;
  }

  public FileChooserImp() {
  }

  @Override
  public File save(String defaultPathFileName) {
    file = new File(defaultPathFileName);
    parentPath = new File(file.getParent() + "/");

    createDirectoryIfNeeded();

    getAbsoluteFilePathAndName(ChooserType.SAVE, null);

    return file;
  }

  public File save(String defaultPathFileName, FileNameExtensionFilter filter) {
    file = new File(defaultPathFileName);
    parentPath = new File(file.getParent() + "/");

    createDirectoryIfNeeded();

    getAbsoluteFilePathAndName(ChooserType.SAVE, filter);

    return file;
  }

  public File open(String defaultPathFileName) {
    file = new File(defaultPathFileName);
    parentPath = new File(file.getParent() + "/");

    createDirectoryIfNeeded();

    getAbsoluteFilePathAndName(ChooserType.OPEN, null);

    return file;
  }

  public File open(String defaultPathFileName, FileNameExtensionFilter filter) {
    file = new File(defaultPathFileName);
    parentPath = new File(file.getParent() + "/");

    createDirectoryIfNeeded();

    getAbsoluteFilePathAndName(ChooserType.OPEN, filter);

    return file;
  }

  private void createDirectoryIfNeeded() {
    if (!parentPath.exists()) {
      if (!parentPath.mkdirs()) {
        try {
          throw new IOException();
        } catch (IOException e) {
          logger.error(String.format("Failed to create directory. %s", parentPath.toString()), e);
        }
      }
    }
  }

  private void getAbsoluteFilePathAndName(ChooserType chooserType, FileNameExtensionFilter filter) {
    JFileChooser j = new JFileChooser(parentPath);
    j.setSelectedFile(file);

    if (filter != null) {
      j.setAcceptAllFileFilterUsed(false);
      j.setFileFilter(filter);
    }

    int r = 0;
    switch (chooserType) {
      case OPEN:
        r = j.showOpenDialog(null);
        break;
      case SAVE:
        r = j.showSaveDialog(null);
        break;
      default:
        r = j.showSaveDialog(null);
        logger.info("invalid chooser type");
    }

    if (r == JFileChooser.APPROVE_OPTION) {
      file = new File(j.getSelectedFile().getAbsolutePath());
    }
  }

  @Override
  public File getFile() {
    return file;
  }
}
