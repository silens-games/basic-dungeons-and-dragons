package file;

import logger.Log;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileCreator {
  private final static Log logger = new Log();

  public static void createIfNotExists(Path path) {
    if (Files.notExists(path)) {
      if (Files.isDirectory(path)) {
        createDirectory(path);
      } else {
        createDirectory(path.getParent());
        createFile(path);
      }
    }
  }

  private static void createDirectory(Path path){
    try {
      Files.createDirectories(path);
    } catch (IOException e) {
      logger.info(String.format("Could not create directory %s", path.toString()), e);
    }
  }

  private static void createFile(Path path){
    try {
      Files.createFile(path);
    } catch (IOException e) {
      logger.info(String.format("Could not create file %s", path.toString()), e);
    }
  }
}
