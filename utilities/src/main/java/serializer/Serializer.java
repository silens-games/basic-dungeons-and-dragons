package serializer;

import com.google.gson.Gson;
import lombok.NonNull;

import javax.annotation.Nullable;
import java.lang.reflect.Type;

public class Serializer {

  public static String toJson(Object object) {
    Gson gson = new Gson();

    return gson.toJson(object);
  }

  public static <T> T fromJson(@Nullable String string,
                               @NonNull Type type) {
    Gson gson = new Gson();

    return gson.fromJson(string, type);
  }
}
