package serializer;

import com.google.gson.Gson;
import lombok.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import serializer.Serializer;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SerializerTest {

  @Data
  @RequiredArgsConstructor
  public class TestDto {

    private final int w;

    private final long x;

    private final String y;

    private final double z;

    protected TestDto() {
      this(
          0,
          2l,
          "3",
          4.4
      );
    }
  }

  private TestDto testDto;

  @BeforeEach
  public void beforeEach(){
    testDto = new TestDto(4, 5l, "6", 7.7);
  }

  @Test
  @DisplayName("Should pass when return toString is correct.")
  public void shouldPassWhenReturnToJsonIsCorrect() {
    // Given
    String expected = "{\"w\":4,\"x\":5,\"y\":\"6\",\"z\":7.7}";

    // When
    String actual = Serializer.toJson(testDto);

    // Then
    assertEquals(expected, actual, String.format("String expected: %s, actual: %s", expected, actual));
  }

  @Test
  @DisplayName("Should pass with fromJson returns correct object")
  public void shouldPassWhenReturnFromJsonIsCorrect() {
    // Given
    String input = "{\"w\":4,\"x\":5,\"y\":\"6\",\"z\":7.7}";

    TestDto expected = testDto;

    // When
    TestDto actual = Serializer.fromJson(input, testDto.getClass());

    Gson gson = new Gson();

    // Then
    assertEquals(expected, actual, String.format("String expected: %s, actual: %s", gson.toJson(expected), gson.toJson(actual)));
  }
}