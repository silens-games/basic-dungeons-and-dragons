package file;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import serializer.Serializer;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

class FileCreatorTest {
  @Test
  public void should(@TempDir Path tempPath){
    // Given
    Path newDirectory = Paths.get(tempPath.toString() + File.separator + "properties" + File.separator);
    Path newFile = Paths.get(tempPath.toString() + File.separator + "radio.properties");
    Path newDirectoryAndFile  = Paths.get(tempPath.toString() + File.separator + "properties2" + File.separator + "radio.properties");

    boolean[] expected = {true, true, true};

    // When
    FileCreator.createIfNotExists(newDirectory);
    FileCreator.createIfNotExists(newFile);
    FileCreator.createIfNotExists(newDirectoryAndFile);

    boolean[] actual = new boolean[3];
    actual[0] = newDirectory.toFile().exists();
    actual[1] = newFile.toFile().exists();
    actual[2] = newDirectoryAndFile.toFile().exists();

    // Then
    assertArrayEquals(expected, actual, String.format("Files exist expected: %s, actual: %s", Serializer.toJson(expected), Serializer.toJson(actual)));
  }
}