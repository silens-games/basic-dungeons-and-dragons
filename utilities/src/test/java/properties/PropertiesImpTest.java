package properties;

import serializer.Serializer;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PropertiesImpTest {

  @Data
  @RequiredArgsConstructor
  public class TestDto {

    private final int w;

    private final long x;

    private final String y;

    private final double z;

    protected TestDto() {
      this(
          0,
          2l,
          "3",
          4.4
      );
    }
  }

  private Map<String, TestDto> expectedMap;


  @BeforeEach
  public void beforeEach(){
    expectedMap = new HashMap<>();

    TestDto testDto1 = new TestDto();
    expectedMap.put(testDto1.getY(), testDto1);
    TestDto testDto2 = new TestDto(9, 8l, "j", 7.7);
    expectedMap.put(testDto2.getY(), testDto2);
  }

  @Test
  @DisplayName("should")
  public void should(@TempDir Path tempDir) {
    // Given
    PropertiesImp<TestDto> propertiesImp = new PropertiesImp(Paths.get(tempDir.toString() + File.separator + "test.properties"));

    propertiesImp.save(expectedMap);

    // When
    Map<String, TestDto> actualMap = propertiesImp.load(TestDto.class);

    // Then
    assertEquals(expectedMap, actualMap, String.format("Test Map expected: %s, actual: %s", Serializer.toJson(expectedMap), Serializer.toJson(actualMap)));
  }
}